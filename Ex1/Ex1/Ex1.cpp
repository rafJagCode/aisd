#include "pch.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <vector>
using namespace std;
void menu() {
	cout << "Wybierz funkcje ktora chcesz przetestowac: \n";
	cout << "0.Exit.\n";
	cout << "1.Palindrom.\n";
	cout << "2.Wypisanie n znakow *.\n";
	cout << "3.Algorytm Hornera.\n";
	cout << "4.Spirala kwadratowa.\n";
	cout << "5.Kwadraty.\n";
}
bool palindrom(string slowo) {
	if (slowo.length() < 2)
		return true;
	else if (slowo[0] == slowo[slowo.length() - 1]) {
		slowo.erase(0, 1);
		slowo.erase(slowo.length() - 1, 1);
		palindrom(slowo);
	}
	else return false;
}
void znaki(int n) {
	if (n > 0) {
		cout << "*";
		znaki(n - 1);
	}
}
int horner(int stopien, int x, int wsp[]) {
	if (stopien == 1)
		return wsp[stopien - 1] * x + wsp[stopien];
	else
		stopien--;
	return x * horner(stopien,x,wsp) + wsp[stopien + 1];
}
char **alok(int rozmiar) {
	char **tab = new char*[rozmiar];
	for (int i = 0; i < rozmiar; i++)
		tab[i] = new char[rozmiar];
	return tab;
}
void zwolnij(char **tab,int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
		delete[] tab[i];
}

void spirala(char **tab, int h, int a, int row = 0, int col=0) {
	if (h <= a) {
		return;
	}
	if (col != 0) {
		for (int i = 0; i < a; i++)
			tab[row][col - a + i] = 1;
	}
	for (int i = 0; i < h - 1; ++i)
		tab[row][col++] = 1;//RIGHT
	for (int i = 0; i < h - 1; ++i)
		tab[row++][col] = 1;//DOWN
	for (int i = 0; i < h - 1; ++i)
		tab[row][col--] = 1;//LEFT
	for (int i = 0; i < h - 1 - a; ++i) {
		tab[row--][col] = 1;//UP
	}
	h -= 2 + 2 * a;
	row ++;
	col = col + a + 1;
	spirala(tab, h, a,row,col);
	}
void wpisz(char** tab, int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			tab[i][j] = ' ';
		}
	}
}
void wyswietl(char **tab, int rozmiar) {
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			cout << tab[i][j];
		}
		cout << endl;
	}
}
void kwadraty(char **tab, int rozmiar, int row = 0,int col=0) {
	int pol1;
	int pol2;
	if(rozmiar<5)
		return;
	for (int i = 0; i < rozmiar - 1; ++i)
		tab[row][col++] = 1;//RIGHT
	for (int i = 0; i < rozmiar - 1; ++i)
		tab[row++][col] = 1;//DOWN
	for (int i = 0; i < rozmiar - 1; ++i)
		tab[row][col--] = 1;//LEFT
	for (int i = 0; i < rozmiar - 1; ++i) {
		if (i == rozmiar / 2)
			pol1 = row;
		tab[row--][col] = 1;//UP
	}
	col++;
	row = pol1;
	rozmiar /= 2;
	for (int i = 0; i < rozmiar; ++i) {
		if (i == rozmiar / 2)
			pol2 = row;
		tab[row--][col++] = 1;//RIGHT UP
	}
	row++;
	col--;
	for (int i = 0; i < rozmiar; ++i)
		tab[row++][col++] = 1;//RIGHT DOWN
	row--;
	col--;
	for (int i = 0; i < rozmiar; ++i)
		tab[row++][col--] = 1;//LEFT DOWN
	row--;
	col++;
	for (int i = 0; i < rozmiar; ++i)
		tab[row--][col--] = 1;//LEFT UP
	kwadraty(tab, rozmiar, pol2, pol2);
}
int main()
{
	char znak;
	string slowo;
	int n;
	int stopien;
	int x;
	int rozmiar;
	int a;
	char **tab;
	while (true) {
		menu();
		znak=_getch();
		switch (znak) {
		case '1':
			cout << "Podaj slowo ktore chcesz sprawdzic czy jest palindromem: ";
			cin >> slowo;
			if (palindrom(slowo)) cout << "Slowo jest palindromem!\n";
			else cout << "Slowo nie jest palindromem!\n";
			break;
		case '2':
			cout << "Ile * chcesz wypisac: ";
			cin >> n;
			znaki(n);
			break;
		case '3': {
			cout << "Podaj stopien wielomianu: ";
			cin >> stopien;
			cout << "Podaj wartosc x: ";
			cin >> x;
			int *wsp = new int[stopien + 1];
			for (int i = 0; i < stopien + 1; i++) {
				cout << "Podaj wspolczynik a" << i << ": ";
				cin >> wsp[i];
			}
			cout<<"Wartosc wielomianu to: "<<horner(stopien, x, wsp);
			delete[] wsp;
		}
		case '4': {
			cout << "Podaj wysokosc spirali: ";
			cin >> rozmiar;
			cout << "Podaj odleglosc pomiedzy spiralami: ";
			cin >> a;
			tab=alok(rozmiar);
			wpisz(tab, rozmiar);
			spirala(tab, rozmiar, a);
			wyswietl(tab, rozmiar);
			zwolnij(tab, rozmiar);
			break;
		}
		case '5': {
			cout << "Podaj wysokosc najwiekszego kwadratu(nieparzysta): ";
			do {
				cin >> rozmiar;
				if (rozmiar % 2 == 0) cout << "Przy wartosci parzystej wyjdzie brzydki rysunek." << endl;
			} while (rozmiar % 2 == 0);
			tab = alok(rozmiar);
			wpisz(tab, rozmiar);
			kwadraty(tab, rozmiar);
			wyswietl(tab, rozmiar);
			zwolnij(tab, rozmiar);
			break;
		}
		case '0':
			exit(0);
			break;
		default:
			cout << "Wpisano bledna wartosc!\n";
			break;
		}
		cout << endl;
		_getch();
		system("pause");
		system("cls");
	}
	return 0;
}
