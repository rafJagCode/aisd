#include "pch.h"
#include <iostream>
#include <conio.h>
#include <vector>
using namespace std;
void wyswietl(vector<int>x) {
	for (int i = 0; i < x.size(); i++)
	{
		cout << x[i] << " ";
	}
	cout << endl;
}
bool czyPowt(int liczba, int *tab, int rozmiar) {
	int licz = 0;
	for (int i = 0; i < rozmiar; i++)
	{
		if (liczba == tab[i]) licz++;
	}
	if (licz > 1) return true;
	else return false;
}
void zad31() {
	int sumaU = 0;
	int sumaD = 0;
	int *tab;
	int rozmiar;
	cout << "Ile liczb chcesz wpisac: ";
	cin >> rozmiar;
	tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		cout << "Podaj liczbe nr " << i + 1 << " :";
		cin >> tab[i];
		if (tab[i] > 0) sumaU += tab[i];
		else sumaD += tab[i];
	}
	cout << "Suma ujemnych wynosi: " << sumaU << endl;
	cout << "Suma dodatnich wynosi: " << sumaD << endl;
	delete[] tab;
}
void zad32(){
	int *tab;
	int rozmiar;
	vector<int>powt;
	cout << "Ile liczb chcesz wpisac: ";
	cin >> rozmiar;
	tab = new int[rozmiar];
	for (int i = 0; i < rozmiar; i++) {
		cout << "Podaj liczbe nr " << i + 1 << " :";
		cin >> tab[i];
	}
	for (int i = 0; i < rozmiar; i++)
	{
		if (czyPowt(tab[i], tab, rozmiar)) powt.push_back(tab[i]);
	}
	wyswietl(powt);
	delete[] tab;
}
void menu() {
	cout << "Wybierz nr zadania:\n";
	cout << "0.EXIT\n";
	cout << "1.\n";
	cout << "2.\n";
	cout << "3.\n";
}
void start() {
	char wybor;
	while (true) {
		menu();
		wybor = _getch();
		switch (wybor)
		{
		case '0':
			exit(0);
			break;
		case '1':
			zad31();
			break;
		case '2':
			zad32();
			break;
		case '3':
			break;
		default:
			cout << "Wybrano zla opcje\n";
			break;
		}
		_getch();
		system("pause");
		system("cls");
	}
}

int main()
{
	start();
	return 0;
}
